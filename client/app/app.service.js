(function(){
    angular
        .module("BookApp")
        .service("BookAppAPI", [
            '$http',
            BookAppAPI
        ]);
    
    function BookAppAPI($http){
        var self = this;

        /* query string '?string='
        self.searchEmployees = function(value){
            return $http.get("/api/employees?keyword=" + value);
        }*/
        //to query more than 1 item
        self.searchBooks = function(value, sortby, itemsPerPage, currentPage){
            return $http.get(`/api/books?keyword=${value}&sortby=${sortby}&itemsPerPage=${itemsPerPage}&currentPage=${currentPage}`);
        }

        // param request
        self.getBook = function(id){
            console.log(id);
            return $http.get("/api/books/" + id)
        }

        // body request
        self.updateBook = function(book){
            console.log(book);
            return $http.put("/api/books",book);
        }


        // parameterized values
        /*
        self.searchEmployees = function(value){
            return $http.get("/api/employees/" + value);
        }*/

        // must learn and master this
//query and params use 'get'
//body use 'post'
// in request, there are 3 ways. one is to post from body, one is to get from parameters, one is by query string
        // post by body over request
        self.addBook = function(book){
            return $http.post("/api/books", book);
        }
// here is requesting employee = the body of the data

        self.deleteBook = function(book_id){
            console.log(book_id);
            return $http.delete("/api/books/"+ book_id);
        }
    }
/* this is requesting in the form of parameters - the specific cell. 
be very clear if there is a / to specify the root folder/file to make changes to*/
})();


// html - controller - http (end of the front end - with post)
