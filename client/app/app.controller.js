(function () {
    angular
        .module("BookApp")
        .controller("BookController", BookController )
        .controller("AddBookCtrl", AddBookCtrl)
        .controller("EditBookCtrl", EditBookCtrl)
        .controller("DeleteBookCtrl", DeleteBookCtrl);

        
    BookController.$inject = ['BookAppAPI', '$uibModal', '$document', '$rootScope', '$scope', "$state", "$http"];
    AddBookCtrl.$inject = ['BookAppAPI', '$rootScope', '$scope'];
    EditBookCtrl.$inject = ['BookAppAPI', 'items', '$rootScope', '$scope'];
    DeleteBookCtrl.$inject = ['$uibModalInstance', 'BookAppAPI','items' ,'$rootScope', '$scope'];

    function DeleteBookCtrl($uibModalInstance, BookAppAPI, items, $rootScope, $scope){
        var self = this;
        //self.items = items;
        self.deleteBook = deleteBook;
        console.log(items);
        BookAppAPI.deleteBook(items).then((result)=>{
            console.log(result.data);
            self.employee =  result.data;
        });

        function deleteBook(){
            console.log("delete book ...");
            BookAppAPI.deleteBook(self.books.id).then((result)=>{
                console.log(result);
                $rootScope.$broadcast('refreshBookList');
                $uibModalInstance.close(self.run);
            }).catch((error)=>{
                console.log(error);
            });
        }
u
    }

    function EditBookCtrl(BookAppAPI, items, $rootScope, $scope){
        console.log("Edit Book Ctrl");
        var self = this;
        self.items = items;
        //initializeCalendar($scope);

        BookAppAPI.getBook(items).then((result)=>{
           console.log(result.data);
           self.book =  result.data;
           
        })

        self.saveBook = saveBook;

        function saveBook(){
            console.log("save book ...");
            console.log(self.book.name);
            console.log(self.book.author);
            console.log(self.book.publish_year);
            console.log(self.book.rev_year);
            BookAppAPI.updateBook(self.book).then((result)=>{
                console.log(result);
                $rootScope.$broadcast('refreshBookList');
             }).catch((error)=>{
                console.log(error);
             })
        }

    }


    function AddBookCtrl(BookAppAPI, $rootScope, $scope){
        var self = this;
        self.saveBook = saveBook;

        self.status = {
            message: ""
        };

        function saveBook(){
            console.log("save book ...");
            console.log(self.book.name);
            console.log(self.book.author);
            console.log(self.book.publish_year);
            console.log(self.book.rev_year);
            BookAppAPI.addBook(self.book).then((result)=>{
                //console.log(result);
                console.log("Add book -> " + result.id);
                self.status.message = "Successfully added " + self.book.name + " " + self.book.rev_year;
                //$state.go('home');
                //$rootScope.$broadcast('refreshBookListFromAdd', result.data);
             }).catch((error)=>{
                console.log(error);
                //self.errorMessage = error;
                self.status.message = "Failed to add";
             })
            //$uibModalInstance.close(self.run);
        }

    }

    
    function BookController(BookAppAPI, $uibModal, $document, $rootScope, $scope, $state, $http){
        var self = this;
        self.format = "M/d/yy h:mm:ss a";
        self.books = [];

        self.searchBooks =  searchBooks;
        self.addBook =  addBook;
        self.editBook = editBook;
        self.deleteBook = deleteBook;
        self.maxsize=5;
        self.totalItems=0;
        self.currentPage = 1;
        self.pageChanged = pageChanged;

    // initialize total items to 0 first. 
        self.itemsPerPage=20;
        self.currentPage=1;
        self.pageChanged = pageChanged;

// because of the duplicate codes of the EMSAppAPI, can modularize as follows:
        function searchAllBooks(searchKeyword, orderby, itemsPerPage, currentPage){
            BookAppAPI.searchBooks(searchKeyword, orderby, itemsPerPage, currentPage).then((results)=>{
                console.log(results);
                self.books = results.data.rows;
                self.totalItems = results.data.count;
                $scope.numPages = Math.ceil(self.totalItems / self.itemsPerPage);
            }).catch((error)=>{
                console.log(error);
            });

        }
 
        function pageChanged(){
            console.log("Page changed " + self.currentPage);
            searchAllBooks(self.searchKeyword, self.orderby, self.itemsPerPage, self.currentPage);
            console.log($scope.numPages);
        }

        $scope.$on("refreshBookList",function(){
            console.log("refresh book list "+ self.searchKeyword);
            searchAllBooks(self.searchKeyword, self.orderby, self.itemsPerPage, self.currentPage);
        });

        $scope.$on("refreshBookListFromAdd",function(event, args){
            console.log("refresh book list from book_id "+ args.id);
            var books = [];
            books.push(args);
            self.searchKeyword = "";
            self.books = books;
        });

        function searchBooks(){
            console.log("search books  ....");
            console.log(self.orderby);
            searchAllBooks(self.searchKeyword, self.orderby, self.itemsPerPage, self.currentPage);
        }

        function addBook(size, parentSelector){
            console.log("post add book  ....");
            var items = [];
            var parentElem = parentSelector ? 
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './views/addbook.html',
                controller: 'AddBookCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return items;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
            
        }
        
        function editBook(id, size, parentSelector){
            console.log("Edit Book...");
            console.log("Book ID > " + id);
            
            var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './views/editBook.html',
                controller: 'EditBookCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return id;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        }

        function deleteBook(id, size, parentSelector){
            console.log("delete book...");
            console.log("Book ID > " + id);
            
            var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './views/deletebook.html',
                controller: 'DeleteBookCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return id;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        }

    }





})();