(function () {
    angular
        .module("BookApp")
        .config(uirouterAppConfig);
    uirouterAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];

    function uirouterAppConfig($stateProvider,$urlRouterProvider){
        $stateProvider
            .state("Home" ,{
                url : '/home',
                templateUrl: "views/home.html",
                controller : 'BookController',
                controllerAs : 'ctrl'
            })
            .state("Add", {
                url: "/add",
                templateUrl: "views/addbook.html",
                controller : 'AddBookCtrl',
                controllerAs : 'ctrl'
            })
            .state("Edit", {
                url: "/edit",
                templateUrl: "views/editbook.html",
                controller : 'EditBookCtrl',
                controllerAs : 'ctrl'
            })


        $urlRouterProvider.otherwise("/home");
    }

})();
