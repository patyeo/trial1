var express = require("express");
var bodyParser = require('body-parser');
var Sequelize = require ("sequelize");

var node_port = process.env.PORT || 4000;
const BOOK_API = "/api/books";

//console.log(Sequelize);
const SQL_USERNAME="root";
const SQL_PASSWORD="A!b2c3d4"
var connection = new Sequelize(
    'day11db',
    SQL_USERNAME,
    SQL_PASSWORD,
    {
        host: 'localhost',
        port: 3306,
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 10,
            min: 0,
            idle: 20000,
            acquire: 20000
        }
    }
);

var Books = require('./model/day11db')(connection, Sequelize);

var app = express();
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({limit: '50mb'}));

app.use(express.static(__dirname + "/../client/"));

// create
// the EMPLOYEE_API (the name of API) are restful(stateless) API and unprotected at this point
app.post(BOOK_API, (req, res)=>{
    console.log(">>> " + JSON.stringify(req.body));
    var book = req.body;
    console.log(book.id)
    Books.create(book).then((result)=>{
        console.log(result);
        res.status(200).json(result);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });
});

// update
app.put(BOOK_API, (req, res)=>{
    console.log(">>> " + JSON.stringify(req.body));
    console.log("update book ..." + req.body);
    console.log(req.body.id);
    var book_id = req.body.id;
    console.log(book_id);
    var whereClause = {limit: 1, where: {id: book_id}};
    Books.findOne(whereClause).then((result)=>{
        result.update(req.body);
        res.status(200).json(result);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });    
});

// retrieve 
app.get(BOOK_API, (req, res)=>{
    console.log("search > " + req.query.keyword);
    var keyword = req.query.keyword;
    var sortby = req.query.sortby;
    var itemsPerPage = parseInt(req.query.itemsPerPage);
    var currentPage = parseInt(req.query.currentPage);
// use parseInt to make it a number and not a string
    var offset = (currentPage-1)*itemsPerPage;
    
    if(sortby=='null'){
        console.log("sortby is null");
        sortby = "ASC";
    }
    console.log(keyword);
    console.log(itemsPerPage);
    console.log(currentPage);
    console.log(typeof offset);

    console.log("sortby"+ sortby);
    console.log(typeof(keyword));
    if(keyword == ''){
        console.log("keyword is empty ?");
    }
    var whereClause = { limit: itemsPerPage, offset: offset, order:[['name', sortby], ['publisher', sortby]] };
    // console.log(keyword.length);
    console.log(keyword !== 'undefined');
    console.log(keyword != undefined);
    console.log(!keyword);
    console.log(keyword.trim().length > 0);

    if((keyword !== 'undefined' || !keyword) && keyword.trim().length > 0){
        console.log("> " + keyword);
        whereClause = {limit: itemsPerPage, offset: offset, order:[['name', sortby], ['publisher', sortby]], where:{name:keyword}};
    }
    console.log(whereClause);
    Books.findAndCountAll(whereClause).then((results)=>{
        res.status(200).json(results);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });    
});

app.get(BOOK_API+"/:id", (req, res)=>{
    console.log("one book ...");
    console.log(req.params.id);
    var id = req.params.id;
    console.log(id);
    var whereClause = {limit: 1, where: {id: id}};
    Books.findOne(whereClause).then((result)=>{
        res.status(200).json(result);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });    
});

app.delete(BOOK_API+"/:id", (req, res)=>{
    console.log("delete book ...");
    console.log(req.params.id);
    var id = req.params.id;
    console.log(id);
    var whereClause = {limit: 1, where: {id: id}};
    Books.findOne(whereClause).then((result)=>{
        result.destroy();
        res.status(200).json({});
    }).catch((error)=>{
        console.log(error);
    });    
});





app.use(function (req, res) {
    res.send("<h1>Page not found</h1>");
});

app.listen(node_port, function () {
    console.log("Server running at http://localhost:" + node_port);
}); 