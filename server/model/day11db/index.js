module.exports = function(connection, Sequelize){
    // employees this have to match the mysql table
    // return Employees object is used within the JS
    var Books = connection.define('books', {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        rev_year: {
            type: Sequelize.INTEGER(4),
            allowNull: false
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        author:{
            type: Sequelize.STRING,
            allowNull: false
        },
        publish_year: {
            type: Sequelize.INTEGER(4),
            allowNull: false
        },
        front_cover: {
            type: Sequelize.STRING,
            defaulValue: "https://cdn.pixabay.com/photo/2014/04/02/10/35/face-303912_960_720.png"
        }

    }, {
        timestamps: false
    });
    return Books;
}